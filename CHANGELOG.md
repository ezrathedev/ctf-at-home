# Version 1.0.1 (2020-07-29)

Fixed a documenation issue about the SSRF challenges

# Version 1.0.0 (2020-07-10)

## Challenges added

* RST & RST 2

# Version 0.6.1 (2020-06-26)

Optimized Rails based Docker images for size with a common base image

# Version 0.6.0 (2020-06-24)

## Challenges added

* Graphicle One and Two (including write-ups)

# Version 0.5.0 (2020-06-23)

* Added write ups for all released challenges

## Challenges added

* Nyan

# Version 0.4.0 (2020-06-02)

* Fixed two typos

## Challenges added

* Tanuki Vault

# Version 0.3.0 (2020-04-15)

## Challenges added

* GTP & OTP

# Version 0.2.2 (2020-04-07)

* Add solutions for all released challenges

# Version 0.2.1 (2020-04-07)

* Fix number of challenges in README.md

# Version 0.2 (2020-04-07)

## Challenges added

* GCM 1 & 2

# Version 0.1 (2020-04-06)

* Initial release

## Challenges added

* SSRF 1-3
* tar2zip
