# Capture the Tanuki!

This repo hosts the run-at-home version of the GitLab CTF which ran from March
16th to March 27th 2020.

# Installation

To run the challenges at home simply clone this repository on your local
machine and run `docker-compose pull` followed by `docker-compose up -d`.
Afterwards the main site should be available at
[`http://capture.local.thetanuki.io`](http://capture.local.thetanuki.io).

In total the `docker-compose pull` command will download about 2 gigabytes of
docker images.

Alternatively, [Multipass](https://multipass.run) can be used to run the
challenges inside a Virtual Machine. Instructions can be found [here](./multipass/README.md). 

# Updates

In order to update and get the latest released challenges use the script
`update.sh` in this folder.
